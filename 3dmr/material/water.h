#include <3dmr/math/linear_algebra.h>
#include <3dmr/render/uniform_buffer.h>
#include <3dmr/render/vertex_array.h>

#ifndef TDMR_MATERIAL_WATER_H
#define TDMR_MATERIAL_WATER_H

#define WATER_UBO_BINDING 3

int water_gen(struct UniformBuffer* water, unsigned int maxDirectional, unsigned int maxCircular);

void water_set_directional_wave(struct UniformBuffer* water, unsigned int i, const Vec2 direction, float amplitude, float speed, float frequency, float steepness);
void water_set_num_directional_wave(struct UniformBuffer* water, unsigned int n);

void water_set_circular_wave(struct UniformBuffer* water, unsigned int i, const Vec2 center, float amplitude, float speed, float frequency, float steepness);
void water_set_circular_wave_amplitude(struct UniformBuffer* water, unsigned int i, float amplitude);
void water_set_circular_wave_cutoff(struct UniformBuffer* water, unsigned int i, float cutoff);
void water_set_num_circular_wave(struct UniformBuffer* water, unsigned int n);

void water_set_time(struct UniformBuffer* water, float t);

GLuint water_vert_shader(const struct UniformBuffer* water);
GLuint water_tesc_shader(const struct UniformBuffer* water);
GLuint water_tese_shader(const struct UniformBuffer* water);

struct Material* water_material_new(const struct UniformBuffer* water, GLuint fragmentShader, void (*frag_load)(GLuint, void*), const void* params);

struct PhongMaterialParams;
struct Material* water_phong_material_new(const struct UniformBuffer* water, const struct PhongMaterialParams* params);

struct PBRMaterialParams;
struct Material* water_pbr_material_new(const struct UniformBuffer* water, const struct PBRMaterialParams* params);

void water_vertex_array_gen(struct VertexArray* dest, const struct UniformBuffer* water, const float* vertices, unsigned int numVertices);

#endif
