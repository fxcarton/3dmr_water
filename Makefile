CFLAGS ?= -std=c89 -pedantic -Wall -g -march=native

DEPS := '3dmr > 0.1'
CFLAGS += -I. $(shell pkg-config --cflags $(DEPS))
LDFLAGS += $(shell pkg-config --libs-only-L --libs-only-other $(DEPS))
LDLIBS += $(shell pkg-config --libs-only-l $(DEPS))
TESTS := $(patsubst %.c,%,$(wildcard test/*.c))

lib3dmr_water.a: $(patsubst %.c,%.o,$(wildcard src/*.c))
	$(AR) rcs $@ $^

test/ubo.h: src/water.c
	grep '^#define WATER_' $< > $@

$(TESTS): test/%: test/%.c lib3dmr_water.a
test/ubo.c: test/ubo.h
test/tropical_beach.hdr: test/Tropical_Beach.zip
	unzip -p $< Tropical_Beach/Tropical_Beach_3k.hdr > $@
test/Tropical_Beach.zip:
	wget -O $@ http://www.hdrlabs.com/sibl/archive/downloads/$(notdir $@)
test/t: | test/tropical_beach.hdr

.PHONY: clean
clean:
	rm -f $(wildcard lib3dmr_water.a src/*.o $(TESTS))

.PHONY: install
DI := $(if $(DESTDIR),$(DESTDIR)/)$(shell pkg-config --variable=includedir 3dmr)
DD := $(if $(DESTDIR),$(DESTDIR)/)$(shell pkg-config --variable=datadir 3dmr)
DL := $(if $(DESTDIR),$(DESTDIR)/)$(shell pkg-config --variable=libdir 3dmr)
install: lib3dmr_water.a
	mkdir -p $(DI) $(DD)/shaders $(DL)
	cp -R 3dmr $(DI)
	cp -R shaders/water $(DD)/shaders
	cp lib3dmr_water.a $(DL)
