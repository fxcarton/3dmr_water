struct Wave {
    vec4 data;
    float amplitude;
    float speed;
    float frequency;
    float steepness;
};

layout(std140) uniform Water {
    uint maxDirectional;
    uint maxCircular;
    uint numDirectional;
    uint numCircular;
    float time;
    Wave waves[MAX_WAVES];
};
