#version 400

#include <camera.glsl>

layout(vertices=3) out;
in vec3 tescPosition[];
out vec3 tesePosition[];

#ifndef TESSEL_MIN_FACTOR
#define TESSEL_MIN_FACTOR 0.0
#endif
#ifndef TESSEL_MAX_FACTOR
#define TESSEL_MAX_FACTOR 6.0
#endif
#ifndef TESSEL_MIN_DIST
#define TESSEL_MIN_DIST 15.0
#endif
#ifndef TESSEL_MAX_DIST
#define TESSEL_MAX_DIST 50.0
#endif

float tess_outer_level(float a, float b) {
    return pow(2.0, mix(TESSEL_MAX_FACTOR, TESSEL_MIN_FACTOR, (clamp((a + b) / 2.0, TESSEL_MIN_DIST, TESSEL_MAX_DIST) - TESSEL_MIN_DIST) / (TESSEL_MAX_DIST - TESSEL_MIN_DIST)));
}

void main() {
    float d0 = distance(cameraPosition, tescPosition[0]);
    float d1 = distance(cameraPosition, tescPosition[1]);
    float d2 = distance(cameraPosition, tescPosition[2]);
    tesePosition[gl_InvocationID] = tescPosition[gl_InvocationID];
    gl_TessLevelOuter[0] = tess_outer_level(d1, d2);
    gl_TessLevelOuter[1] = tess_outer_level(d0, d2);
    gl_TessLevelOuter[2] = tess_outer_level(d0, d1);
    gl_TessLevelInner[0] = max(max(gl_TessLevelOuter[0], gl_TessLevelOuter[1]), gl_TessLevelOuter[2]);
}
