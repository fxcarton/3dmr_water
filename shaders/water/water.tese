#version 400

#include <camera.glsl>
#include "water.glsl"

layout(triangles, equal_spacing, ccw) in;

in vec3 tesePosition[];
out vec3 surfelPosition;
out vec3 surfelNormal;

mat2x3 displacement_normal_wave(vec2 P, vec2 D, float A, float Q, float S, float w) {
    float QA = Q / w; // (Q / (w * A)) * A
    float phi = dot(w * D, P) + S * w * time;
    float c = cos(phi);
    float s = sin(phi);
    return mat2x3(vec3(QA * c * D, A * s), vec3(0, 0, 1) - vec3(D, Q) * w * A * vec3(c, c, s));
}

mat2x3 displacement_normal_directional_wave(vec2 position, Wave wave, float t) {
    vec2 direction = wave.data.xy;
    return displacement_normal_wave(position, -direction, wave.amplitude, wave.steepness, wave.speed, wave.frequency);
}

mat2x3 displacement_normal_circular_wave(vec2 position, Wave wave, float t) {
    vec2 center = wave.data.xy;
    vec2 pos = position - center;
    float dist = length(pos);
    vec2 direction = pos / dist;
    if (dist > wave.data.z) {
        return mat2x3(vec3(0.0), vec3(0.0));
    }
    return displacement_normal_wave(pos, -direction, wave.amplitude, wave.steepness, wave.speed, wave.frequency);
}

void main() {
    mat2x3 DN = mat2x3(vec3(0.0), vec3(0.0));
    float ln;
    uint i;
    surfelPosition = gl_TessCoord.x * tesePosition[0] + gl_TessCoord.y * tesePosition[1] + gl_TessCoord.z * tesePosition[2];
    for (i = 0U; i < numDirectional; i++) {
        DN += displacement_normal_directional_wave(surfelPosition.xz, waves[i], time);
    }
    for (i = 0U; i < numCircular; i++) {
        DN += displacement_normal_circular_wave(surfelPosition.xz, waves[maxDirectional + i], time);
    }
    surfelPosition += DN[0].xzy;
    surfelNormal = DN[1].xzy;
    ln = length(surfelNormal);
    if (ln > 0.0) {
        surfelNormal /= ln;
    } else {
        surfelNormal = vec3(0.0, 1.0, 0.0);
    }
    gl_Position = projection * view * vec4(surfelPosition, 1.0);
}
