#version 400
in vec3 in_Vertex;
out vec3 tescPosition;
uniform mat4 model;

void main() {
    tescPosition = (model * vec4(in_Vertex, 1.0)).xyz;
}
