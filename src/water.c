#include <stdio.h>
#include <stdint.h>
#include <inttypes.h>
#include <string.h>
#include <3dmr/shaders.h>
#include <3dmr/material/phong.h>
#include <3dmr/material/pbr.h>
#include <3dmr/material/water.h>
#include <3dmr/render/material.h>
#include <3dmr/render/shader.h>

#define WATER_MAX_DIRECTIONAL_OFFSET 0
#define WATER_MAX_CIRCULAR_OFFSET (sizeof(uint32_t))
#define WATER_NUM_DIRECTIONAL_OFFSET (2 * sizeof(uint32_t))
#define WATER_NUM_CIRCULAR_OFFSET (3 * sizeof(uint32_t))
#define WATER_TIME_OFFSET (4 * sizeof(uint32_t))

#define WATER_WAVE_SIZE (2 * sizeof(Vec4))
#define WATER_DIRECTIONAL_WAVE_OFFSET(i) (WATER_WAVE_SIZE + (i) * WATER_WAVE_SIZE)
#define WATER_CIRCULAR_WAVE_OFFSET(md, i) (WATER_DIRECTIONAL_WAVE_OFFSET(md) + (i) * WATER_WAVE_SIZE)
#define WATER_SIZE(md, mc) (WATER_CIRCULAR_WAVE_OFFSET(md, mc))

#define WATER_DIRECTIONAL_WAVE_DIRECTION_OFFSET 0
#define WATER_CIRCULAR_WAVE_CENTER_OFFSET 0
#define WATER_CIRCULAR_WAVE_CUTOFF_OFFSET (sizeof(Vec2))
#define WATER_WAVE_AMPLITUDE_OFFSET (sizeof(Vec4))
#define WATER_WAVE_SPEED_OFFSET (WATER_WAVE_AMPLITUDE_OFFSET + sizeof(float))
#define WATER_WAVE_FREQUENCY_OFFSET (WATER_WAVE_SPEED_OFFSET + sizeof(float))
#define WATER_WAVE_STEEPNESS_OFFSET (WATER_WAVE_FREQUENCY_OFFSET + sizeof(float))

int water_gen(struct UniformBuffer* water, unsigned int maxDirectional, unsigned int maxCircular) {
    uint32_t md = maxDirectional;
    uint32_t mc = maxCircular;
    uint32_t n;
    uint32_t zero = 0;
    float zerof = 0;
    if (md > ((uint32_t)-1) - mc
     || (n = (md + mc)) > ((uint32_t)-1) / WATER_WAVE_SIZE
     || (n *= WATER_WAVE_SIZE) > ((uint32_t)-1) - WATER_WAVE_SIZE
     || !uniform_buffer_gen(WATER_SIZE(md, mc), water)) {
        return 0;
    }
    uniform_buffer_update(water, WATER_MAX_DIRECTIONAL_OFFSET, sizeof(md), &md);
    uniform_buffer_update(water, WATER_MAX_CIRCULAR_OFFSET, sizeof(mc), &mc);
    uniform_buffer_update(water, WATER_NUM_DIRECTIONAL_OFFSET, sizeof(zero), &zero);
    uniform_buffer_update(water, WATER_NUM_CIRCULAR_OFFSET, sizeof(zero), &zero);
    uniform_buffer_update(water, WATER_TIME_OFFSET, sizeof(zerof), &zerof);
    return 1;
}

static void get_maximums(const struct UniformBuffer* water, uint32_t* md, uint32_t* mc) {
    memcpy(md, ((unsigned char*)water->cache) + WATER_MAX_DIRECTIONAL_OFFSET, sizeof(*md));
    memcpy(mc, ((unsigned char*)water->cache) + WATER_MAX_CIRCULAR_OFFSET, sizeof(*mc));
}

void water_set_directional_wave(struct UniformBuffer* water, unsigned int i, const Vec2 direction, float amplitude, float speed, float frequency, float steepness) {
    uint32_t md, mc;
    get_maximums(water, &md, &mc);
    if (i > md) return;
    uniform_buffer_update(water, WATER_DIRECTIONAL_WAVE_OFFSET(i) + WATER_DIRECTIONAL_WAVE_DIRECTION_OFFSET, sizeof(Vec2), direction);
    uniform_buffer_update(water, WATER_DIRECTIONAL_WAVE_OFFSET(i) + WATER_WAVE_AMPLITUDE_OFFSET, sizeof(float), &amplitude);
    uniform_buffer_update(water, WATER_DIRECTIONAL_WAVE_OFFSET(i) + WATER_WAVE_SPEED_OFFSET, sizeof(float), &speed);
    uniform_buffer_update(water, WATER_DIRECTIONAL_WAVE_OFFSET(i) + WATER_WAVE_FREQUENCY_OFFSET, sizeof(float), &frequency);
    uniform_buffer_update(water, WATER_DIRECTIONAL_WAVE_OFFSET(i) + WATER_WAVE_STEEPNESS_OFFSET, sizeof(float), &steepness);
}

void water_set_num_directional_wave(struct UniformBuffer* water, unsigned int n) {
    uint32_t md, mc, nd = n;
    get_maximums(water, &md, &mc);
    if (nd > md) {
        nd = md;
    }
    uniform_buffer_update(water, WATER_NUM_DIRECTIONAL_OFFSET, sizeof(nd), &nd);
}

void water_set_circular_wave(struct UniformBuffer* water, unsigned int i, const Vec2 center, float amplitude, float speed, float frequency, float steepness) {
    float zero = 0;
    uint32_t md, mc;
    get_maximums(water, &md, &mc);
    if (i > mc) return;
    uniform_buffer_update(water, WATER_CIRCULAR_WAVE_OFFSET(md, i) + WATER_CIRCULAR_WAVE_CENTER_OFFSET, sizeof(Vec2), center);
    uniform_buffer_update(water, WATER_CIRCULAR_WAVE_OFFSET(md, i) + WATER_CIRCULAR_WAVE_CUTOFF_OFFSET, sizeof(float), &zero);
    uniform_buffer_update(water, WATER_CIRCULAR_WAVE_OFFSET(md, i) + WATER_WAVE_AMPLITUDE_OFFSET, sizeof(float), &amplitude);
    uniform_buffer_update(water, WATER_CIRCULAR_WAVE_OFFSET(md, i) + WATER_WAVE_SPEED_OFFSET, sizeof(float), &speed);
    uniform_buffer_update(water, WATER_CIRCULAR_WAVE_OFFSET(md, i) + WATER_WAVE_FREQUENCY_OFFSET, sizeof(float), &frequency);
    uniform_buffer_update(water, WATER_CIRCULAR_WAVE_OFFSET(md, i) + WATER_WAVE_STEEPNESS_OFFSET, sizeof(float), &steepness);
}

void water_set_circular_wave_amplitude(struct UniformBuffer* water, unsigned int i, float amplitude) {
    uint32_t md, mc;
    get_maximums(water, &md, &mc);
    if (i > mc) return;
    uniform_buffer_update(water, WATER_CIRCULAR_WAVE_OFFSET(md, i) + WATER_WAVE_AMPLITUDE_OFFSET, sizeof(float), &amplitude);
}

void water_set_circular_wave_cutoff(struct UniformBuffer* water, unsigned int i, float cutoff) {
    uint32_t md, mc;
    get_maximums(water, &md, &mc);
    if (i > mc) return;
    uniform_buffer_update(water, WATER_CIRCULAR_WAVE_OFFSET(md, i) + WATER_CIRCULAR_WAVE_CUTOFF_OFFSET, sizeof(float), &cutoff);
}

void water_set_num_circular_wave(struct UniformBuffer* water, unsigned int n) {
    uint32_t md, mc, nc = n;
    get_maximums(water, &md, &mc);
    if (nc > mc) {
        nc = mc;
    }
    uniform_buffer_update(water, WATER_NUM_CIRCULAR_OFFSET, sizeof(nc), &nc);
}

void water_set_time(struct UniformBuffer* water, float t) {
    uniform_buffer_update(water, WATER_TIME_OFFSET, sizeof(t), &t);
}

GLuint water_vert_shader(const struct UniformBuffer* water) {
    return shader_find_compile("water/water.vert", GL_VERTEX_SHADER, &tdmrShaderRootPath, 1, NULL, 0);
}

GLuint water_tesc_shader(const struct UniformBuffer* water) {
    return shader_find_compile("water/water.tesc", GL_TESS_CONTROL_SHADER, &tdmrShaderRootPath, 1, NULL, 0);
}

GLuint water_tese_shader(const struct UniformBuffer* water) {
    char buffer[32];
    const char* defines[] = {"MAX_WAVES", 0};
    uint32_t md, mc, n;
    get_maximums(water, &md, &mc);
    n = md + mc;
    sprintf(buffer, "%"PRIu32, n);
    defines[1] = buffer;
    return shader_find_compile("water/water.tese", GL_TESS_EVALUATION_SHADER, &tdmrShaderRootPath, 1, defines, 1);
}

struct Material* water_material_new(const struct UniformBuffer* water, GLuint fragmentShader, void (*frag_load)(GLuint, void*), const void* params) {
    struct Material* m = NULL;
    GLuint shaders[4];
    shaders[0] = water_vert_shader(water);
    shaders[1] = water_tesc_shader(water);
    shaders[2] = water_tese_shader(water);
    shaders[3] = fragmentShader;
    if (shaders[0] && shaders[1] && shaders[2] && shaders[3]) {
        if ((m = material_new_from_shaders(shaders, 4, frag_load, (void*)params, GL_FILL))) {
            glUniformBlockBinding(m->program, glGetUniformBlockIndex(m->program, "Water"), WATER_UBO_BINDING);
        }
    }
    if (shaders[0]) glDeleteShader(shaders[0]);
    if (shaders[1]) glDeleteShader(shaders[1]);
    if (shaders[2]) glDeleteShader(shaders[2]);
    return m;
}

struct Material* water_phong_material_new(const struct UniformBuffer* water, const struct PhongMaterialParams* params) {
    struct Material* m;
    GLuint fragmentShader = phong_shader_new(params);
    if (!fragmentShader) return NULL;
    m = water_material_new(water, fragmentShader, phong_load, params);
    glDeleteShader(fragmentShader);
    return m;
}

struct Material* water_pbr_material_new(const struct UniformBuffer* water, const struct PBRMaterialParams* params) {
    struct Material* m;
    GLuint fragmentShader = pbr_shader_new(params);
    if (!fragmentShader) return NULL;
    m = water_material_new(water, fragmentShader, pbr_load, params);
    glDeleteShader(fragmentShader);
    return m;
}

static void water_load(void* p) {
    struct UniformBuffer* water = p;
    uniform_buffer_send(water);
    glBindBufferRange(GL_UNIFORM_BUFFER, WATER_UBO_BINDING, water->ubo, 0, water->size);
}

void water_vertex_array_gen(struct VertexArray* dest, const struct UniformBuffer* water, const float* vertices, unsigned int numVertices) {
    struct Mesh mesh;
    mesh.numVertices = numVertices;
    mesh.vertices = (float*)vertices;
    mesh.numIndices = 0;
    mesh.indices = NULL;
    mesh.flags = 0;
    vertex_array_gen(&mesh, dest);
    dest->load = water_load;
    dest->params = (void*)water;
}
