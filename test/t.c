#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <3dmr/skybox.h>
#include <3dmr/img/png.h>
#include <3dmr/material/solid.h>
#include <3dmr/material/pbr.h>
#include <3dmr/material/water.h>
#include <3dmr/mesh/icosphere.h>
#include <3dmr/render/shader.h>
#include <3dmr/render/viewer.h>
#include <3dmr/render/camera_buffer_object.h>
#include <3dmr/render/lights_buffer_object.h>
#include <GLFW/glfw3.h>

#define N 128
#define S 16
#define SKYBOX "test/tropical_beach.hdr"

static void vclose(struct Viewer* v, void* p) {
    *(int*)p = 0;
}

int main(void) {
    struct Mesh sphere;
    struct Skybox skybox;
    struct IBL ibl;
    struct PBRMaterialParams watParams, ballParams;
    struct VertexArray va, ball;
    struct UniformBuffer water, camera, lights;
    struct Viewer* v;
    struct Material *material, *matBall;
    float* vertices;
    const unsigned char d[2][6] = {
        {0, 1, 1, 1, 0, 0},
        {0, 0, 1, 1, 0, 1}
    };
    Mat4 model;
    Mat3 invNormals;
    Vec3 b = {S * N / 2, 20, S * N / 2};
    unsigned int x, y, i;
    float t = 0, dt, dz = 0, start, cwSpeed = 8, cwFreq = 0.5, cwAmpl = 0.5, bRadius = 1;
    int running = 1, ret = 1, ok = 0, bd = 0;
    GLuint skytex;

    pbr_material_params_init(&watParams);
    material_param_set_vec3_elems(&watParams.albedo, 0.0, 0.05, 0.2);
    material_param_set_float_constant(&watParams.metalness, 0);
    material_param_set_float_constant(&watParams.roughness, 0.2);

    pbr_material_params_init(&ballParams);
    material_param_set_vec3_elems(&ballParams.albedo, 0.5, 0, 0);
    material_param_set_float_constant(&ballParams.metalness, 0);
    material_param_set_float_constant(&ballParams.roughness, 0.3);

    if (!(v = viewer_new(960, 720, "water"))) goto end;
    ret++;
    v->callbackData = &running;
    v->close_callback = vclose;

    if (!lights_buffer_object_gen(&lights)) goto end;
    ret++;
    {
        struct AmbientLight l = {{0}};
        lights_buffer_object_update_ambient(&lights, &l);
    }
    {
        struct DirectionalLight l = {{0, -1.732 / 2, -0.5}, {1, 1, 1}};
        lights_buffer_object_update_dlight(&lights, &l, 0);
    }
    lights_buffer_object_update_ndlight(&lights, 1);
    lights_buffer_object_update_nplight(&lights, 0);
    lights_buffer_object_update_nslight(&lights, 0);
    uniform_buffer_send(&lights);

    if (!camera_buffer_object_gen(&camera)) goto end;
    ret++;
    {
        Mat4 m;
        Vec3 pos = {S * N / 2, 5, S * N / 2}, dir;
        Quaternion orient, qx, qy;
        quaternion_set_axis_angle(qx, VEC3_AXIS_X, -M_PI / 24);
        quaternion_set_axis_angle(qy, VEC3_AXIS_Y, -M_PI / 3);
        quaternion_mul(orient, qy, qx);
        camera_projection(4.0 / 3.0, 1.04, 0.1, 1000, m);
        camera_buffer_object_update_projection(&camera, MAT_CONST_CAST(m));
        camera_view(pos, orient, m);
        camera_get_backward(MAT_CONST_CAST(m), dir);
        dir[1] = 0;
        scale3v(dir, -20.0);
        incr3v(b, dir);
        camera_buffer_object_update_view(&camera, MAT_CONST_CAST(m));
        camera_buffer_object_update_position(&camera, pos);
        uniform_buffer_send(&camera);
    }

    if (!water_gen(&water, 4, 4)) goto end;
    ret++;
    {
        Vec2 c;
        c[0] = b[0];
        c[1] = b[2];
        water_set_directional_wave(&water, 0, VEC3_AXIS_X, 0.05, 1, 1, 0.5);
        water_set_directional_wave(&water, 1, VEC3_AXIS_Y, 0.025, 2, 1.5, 0.6);
        water_set_num_directional_wave(&water, 2);
        water_set_circular_wave(&water, 0, c, cwAmpl, cwSpeed, cwFreq, 0.1);
        water_set_num_circular_wave(&water, 0);
    }

    if (!(vertices = malloc(N * N * 6 * 3 * sizeof(float)))) goto end;
    ret++;
    for (y = 0; y < N; y++) {
        for (x = 0; x < N; x++) {
            for (i = 0; i < 6; i++) {
                vertices[3 * (6 * (N * y + x) + i)    ] = S * (x + d[0][i]);
                vertices[3 * (6 * (N * y + x) + i) + 1] = 0;
                vertices[3 * (6 * (N * y + x) + i) + 2] = S * (y + d[1][i]);
            }
        }
    }
    water_vertex_array_gen(&va, &water, vertices, 6 * N * N);
    free(vertices);

    if (!(material = water_pbr_material_new(&water, &watParams))) goto end;
    ret++;
    material->polygonMode = GL_FILL;

    if (!(skytex = skybox_load_texture_hdr_equirect(SKYBOX, 1024))) goto end;
    ret++;

    if (!compute_ibl(skytex, 32, 1024, 5, 256, &ibl)) goto end;
    ret++;
    watParams.ibl = &ibl;

    if (!skybox_create(skytex, &skybox)) goto end;
    ret++;

    if (!make_icosphere(&sphere, bRadius, 4)) goto end;
    vertex_array_gen(&sphere, &ball);
    mesh_free(&sphere);
    if (!ball.vao) goto end;
    ret++;

    if (!(matBall = pbr_material_new(ball.flags, &ballParams))) goto end;
    ret++;

    ok = 1;
    glfwSwapInterval(1);
    while (running) {
        dt = viewer_next_frame(v);
        viewer_process_events(v);
        t += dt;

        water_set_time(&water, t);
        material_use(material);
        load_id4(model);
        load_id3(invNormals);
        material_set_matrices(material, model, invNormals);
        va.load(va.params);
        glBindVertexArray(va.vao);
        glDrawArrays(GL_PATCHES, 0, va.numVertices);
        glBindVertexArray(0);

        if (b[1] > -bRadius) {
            material_use(matBall);
            load_id4(model);
            load_id3(invNormals);
            memcpy(model[3], b, sizeof(Vec3));
            material_set_matrices(matBall, model, invNormals);
            vertex_array_render(&ball);
            dz += -0.02 * dt;
            b[1] += dz;
        }

        skybox_render(&skybox);

        switch (bd) {
            case 0:
                if (b[1] < bRadius) {
                    bd = 1;
                    water_set_num_circular_wave(&water, 1);
                    start = t;
                }
                break;
            case 1:
                cwAmpl -= 0.0005;
                if (cwAmpl < 0) {
                    bd = 2;
                    water_set_num_circular_wave(&water, 0);
                    start = t;
                } else {
                    water_set_circular_wave_amplitude(&water, 0, cwAmpl);
                    water_set_circular_wave_cutoff(&water, 0, cwSpeed * (t - start));
                }
                break;
            case 2:
                if (t - start > 5) {
                    fputs("done\n", stdout);
                    goto end;
                }
                break;
        }
    }

end:
    if (ret > 1) viewer_free(v);
    if (ret > 2) uniform_buffer_del(&lights);
    if (ret > 3) uniform_buffer_del(&camera);
    if (ret > 4) uniform_buffer_del(&water);
    if (ret > 5) vertex_array_del(&va);
    if (ret > 6) free(material);
    if (ret > 7) glDeleteTextures(1, &skytex);
    if (ret > 8) {
        GLuint textures[3];
        textures[0] = ibl.irradianceMap;
        textures[1] = ibl.specularMap;
        textures[2] = ibl.specularBrdf;
        glDeleteTextures(3, textures);
    }
    if (ret > 9) skybox_destroy(&skybox);
    if (ret > 10) vertex_array_del(&ball);
    if (ret > 11) free(matBall);
    if (ok) ret = 0;
    return ret;
}
