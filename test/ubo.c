#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <3dmr/render/shader.h>
#include <3dmr/render/viewer.h>
#include <3dmr/material/water.h>
#include "ubo.h"

#define MD 2
#define MC 2

int test_ubo(GLuint prog, GLuint ubo, const char* uboName, unsigned long (*get_offset)(const char*)) {
    char* name;
    unsigned long offset, expected;
    GLuint i, id, numUniforms;
    GLint tmp;
    int ret = 1;

    id = glGetUniformBlockIndex(prog, uboName);
    if (id == GL_INVALID_INDEX) return 0;
    glGetProgramiv(prog, GL_ACTIVE_UNIFORMS, &tmp);
    if (tmp < 0) return 0;
    numUniforms = tmp;
    for (i = 0; i < numUniforms; i++) {
        glGetActiveUniformsiv(prog, 1, &i, GL_UNIFORM_BLOCK_INDEX, &tmp);
        if (tmp < 0 || ((GLuint)tmp) != id) continue;
        glGetActiveUniformsiv(prog, 1, &i, GL_UNIFORM_NAME_LENGTH, &tmp);
        if (tmp < 0 || !(name = malloc(((GLuint)tmp) + 1))) return 0;
        glGetActiveUniformName(prog, i, ((GLuint)tmp) + 1, NULL, name);
        name[tmp] = 0;
        offset = get_offset(name);
        glGetActiveUniformsiv(prog, 1, &i, GL_UNIFORM_OFFSET, &tmp);
        if (tmp < 0) {
            free(name);
            return 0;
        }
        expected = tmp;
        printf("%c %-44s %016lX %016lX\n", (offset == expected) ? ' ' : '>', name, offset, expected);
        ret = ret && (offset == expected);
        free(name);
    }
    return ret;
}

unsigned long wave_offset(unsigned long b, const char* name) {
    if (!strcmp(name, "data")) {
        return b;
    } else if (!strcmp(name, "amplitude")) {
        return b + WATER_WAVE_AMPLITUDE_OFFSET;
    } else if (!strcmp(name, "speed")) {
        return b + WATER_WAVE_SPEED_OFFSET;
    } else if (!strcmp(name, "frequency")) {
        return b + WATER_WAVE_FREQUENCY_OFFSET;
    } else if (!strcmp(name, "steepness")) {
        return b + WATER_WAVE_STEEPNESS_OFFSET;
    }
    return -1;
}

unsigned long water_offset(const char* name) {
    unsigned long i, b;

    if (!strcmp(name, "maxDirectional")) {
        return WATER_MAX_DIRECTIONAL_OFFSET;
    } else if (!strcmp(name, "maxCircular")) {
        return WATER_MAX_CIRCULAR_OFFSET;
    } else if (!strcmp(name, "numDirectional")) {
        return WATER_NUM_DIRECTIONAL_OFFSET;
    } else if (!strcmp(name, "numCircular")) {
        return WATER_NUM_CIRCULAR_OFFSET;
    } else if (!strcmp(name, "time")) {
        return WATER_TIME_OFFSET;
    } else if (!strncmp(name, "waves[", 6)) {
        i = strtoul(name + 6, (char**)&name, 10);
        b = (i < MD) ? (WATER_DIRECTIONAL_WAVE_OFFSET(i)) : (WATER_CIRCULAR_WAVE_OFFSET(MD, i - MD));
        if (*name++ != ']') return -1;
        if (*name++ != '.') return -1;
        return wave_offset(b, name);
    }
    return -1;
}

int test_water(GLuint prog) {
    struct UniformBuffer u;
    int ret;

    if (!water_gen(&u, MD, MC)) return 0;
    ret = test_ubo(prog, u.ubo, "Water", water_offset);
    uniform_buffer_del(&u);
    return ret;
}

#define STR_(x) #x
#define STR(x) STR_(x)

int main() {
    static const char* defines[] = {"MAX_WAVES", STR(MD) " + " STR(MC)};
    struct Viewer* viewer;
    const char* shaderPath[] = {"shaders", TDMR_SHADERS_PATH};
    GLuint prog, shaders[4];
    int ret = 1;

    if ((viewer = viewer_new(640, 480, ""))) {
        shaders[0] = shader_find_compile("water/water.vert", GL_VERTEX_SHADER, shaderPath, 2, defines, sizeof(defines) / (2 * sizeof(*defines)));
        shaders[1] = shader_find_compile("water/water.tesc", GL_TESS_CONTROL_SHADER, shaderPath, 2, defines, sizeof(defines) / (2 * sizeof(*defines)));
        shaders[2] = shader_find_compile("water/water.tese", GL_TESS_EVALUATION_SHADER, shaderPath, 2, defines, sizeof(defines) / (2 * sizeof(*defines)));
        shaders[3] = shader_compile("test.frag", GL_FRAGMENT_SHADER, shaderPath, 2, defines, sizeof(defines) / (2 * sizeof(*defines)));
        if ((prog = shader_link(shaders, 4))) {
            printf("================================================================================\n");
            printf("  %-44s %-16s %-16s\n", "name", "offset", "expected");
            printf("--------------------------------------------------------------------------------\n");
            ret = !test_water(prog);
            printf("================================================================================\n");
            glDeleteProgram(prog);
        }
        viewer_free(viewer);
    }
    return ret;
}
